/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import {Store} from './redux';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Colors} from './services';

const App = () => {
  return (
    <SafeAreaProvider>
      <Provider store={Store}>
        <NavigationContainer>
          <Router />
        </NavigationContainer>
      </Provider>
    </SafeAreaProvider>
  );
};

export default App;

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: Colors.statusBar,
  },
});
