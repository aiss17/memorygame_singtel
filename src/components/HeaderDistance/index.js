import React from 'react';
import {Dimensions, Platform, View} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';

const {width, height} = Dimensions.get('window');

const HeaderDistance = ({latitude, longitude, style}) => {
  return (
    <View>
      {Platform.OS === 'ios' && (
        <View
          // colors={colors.gradient}
          style={{
            height: getStatusBarHeight(),
          }}></View>
      )}
    </View>
  );
};

export default HeaderDistance;
