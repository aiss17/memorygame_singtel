import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  Platform,
  Dimensions,
  Button,
} from 'react-native';
import {
  createNativeStackNavigator,
  Header,
} from '@react-navigation/native-stack';
import {GamePlay} from '../pages';
const {width, height} = Dimensions.get('window');

const Stack = createNativeStackNavigator();
const Router = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
      }}
      //   initialRouteName="AuthLoading"
    >
      <Stack.Screen
        name="AuthLoading"
        component={GamePlay}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  //   shadow: {
  //     shadowColor: colors.disable,
  //     shadowOffset: {
  //       width: 0,
  //       height: 10,
  //     },
  //     shadowOpacity: 0.25,
  //     shadowRadius: 3.5,
  //     elevation: 5,
  //   },
});

export default Router;
