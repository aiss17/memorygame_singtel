import React, {useEffect, useState} from 'react';
import {
  BackHandler,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {HeaderDistance} from '../../components';
import {Colors} from '../../services';

const GamePlay = ({navigation}) => {
  const [step, setStep] = useState(0);
  const [cards, setCards] = useState([]);

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const generate = () => {
    let generateNumber = [];

    for (var i = 1; i <= 100; i++) {
      generateNumber.push(i);
    }

    return generateNumber;
  };

  const shuffleNumber = () => {
    let numberShuffle = [];
    const generateNumber = generate();

    const firstShuffle = generateNumber.sort(() => Math.random() - 0.5);

    for (let i = 1; i <= 6; i++) {
      numberShuffle.push(firstShuffle[i]);
    }
    const merge = [...numberShuffle, ...numberShuffle];
    const lastShuffle = merge
      .sort(() => Math.random() - 0.5)
      .map(value => ({nomor: value, id: Math.random()}));

    setCards(lastShuffle);

    console.log('hehe => ', lastShuffle);
  };

  useEffect(() => {
    shuffleNumber();
    BackHandler.addEventListener('hardwareBackPress', backPressed);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', backPressed);
  }, []);

  return (
    <View style={styles.container}>
      <HeaderDistance />

      <View style={styles.headerViewCard}>
        <TouchableOpacity onPress={() => shuffleNumber()}>
          <Text style={styles.textRestart}>Restart</Text>
        </TouchableOpacity>
        <View style={styles.viewStep}>
          <Text style={styles.textTitleStep}>STEP :</Text>
          <Text style={styles.textValueStep}>{step}</Text>
        </View>
      </View>

      <FlatList
        style={{margin: 5}}
        data={cards}
        numColumns={3}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => {
          return (
            <View
              style={{
                margin: 20,
                flexDirection: 'row',
                height: 50,
                width: 50,
                borderWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'red'}}>{item.nomor}</Text>
              {/* <Text style={{color: 'red'}}>Haha</Text> */}
            </View>
          );
        }}
      />
    </View>
  );
};

export default GamePlay;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerViewCard: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textRestart: {
    color: Colors.card,
  },
  viewStep: {
    flexDirection: 'row',
  },
  textTitleStep: {
    marginRight: 5,
    fontWeight: 'bold',
  },
  textValueStep: {},
});
