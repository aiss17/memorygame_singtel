const SET_LOGIN = (value) => {
    return { type: "SET_LOGIN", inputValue: value };
};

export { SET_LOGIN };
