import Store from "./store";
import { SET_LOGIN } from "./action";

export { SET_LOGIN, Store };
