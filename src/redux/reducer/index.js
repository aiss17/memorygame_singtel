import { combineReducers } from "redux";
import { LoginReducer } from "./LoginReducer";

const reducer = combineReducers({
    LoginReducer,
});

export default reducer;
