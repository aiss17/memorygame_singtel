const initialStateHome = {
    nup: "",
    nama: "",
    jabatan: "",
    email: "",
    isLogin: false,
};

// reducer
export const LoginReducer = (state = initialStateHome, action) => {
    switch (action.type) {
        case "SET_LOGIN":
            console.log("update ketika login dan ", action.inputValue);
            return {
                ...state,
                nup: action.inputValue.nup,
                nama: action.inputValue.nama,
                jabatan: action.inputValue.jabatan,
                email: action.inputValue.email,
                isLogin: true,
            };

        case "SET_NUP":
            return {
                ...state,
                nup: action.inputValue,
            };

        case "SET_NAME":
            console.log("update key dan ", action.inputValue);
            return {
                ...state,
                nama: action.inputValue,
            };

        case "SET_EMAIL":
            console.log("update email dan ", action.inputValue);
            return {
                ...state,
                email: action.inputValue,
            };

        case "SET_JABATAN":
            console.log("update email dan ", action.inputValue);
            return {
                ...state,
                jabatan: action.inputValue,
            };

        default:
            return state;
    }
};
